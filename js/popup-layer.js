function Popup(){
	this.width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
	this.height = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
	this.options = {
		title: null, 
		href: null,
		width: 1200,
		height: 700,
		zIndex: 2000000000,
		index: null,
		close: function(){},
		level: function(){}
	};
	this.idLayer = document.querySelector('#unknown .popup-layer #layer');
	this.dataName = null;
};

Popup.prototype = {
	open: function(options){
		for(k in options){
			this.options[k] = options[k];
		};
		this.dataName = 'layer-box-n' + options.index;
		if(this.check(this.dataName)){
			this.idLayer.appendChild(this.create(this.options));
		}else{
			this.zIndex(this.dataName);
			this.small(options.index, false);
		}
	},
	close: function(index){
		var name = 'layer-box-n' + index,
			layer = this.idLayer.querySelector('.layer[data-name=' + name + ']');
		if(layer instanceof HTMLElement){
			layer.parentNode.removeChild(layer);
			this.options.close(index);
		}
	},
	level: function(index){
		var name = 'layer-box-n' + index;
		this.zIndex(name);
	},
	check: function(name){
		var layer = this.idLayer.querySelector('.layer[data-name=' + name + ']');
		if(layer instanceof HTMLElement){
			return false;
		}
		return true;
	},
	zIndex: function(name){
		var layers = this.idLayer.querySelectorAll('.layer'),
			layerLen = layers.length;
		for(var i=0; i<layerLen; i++){
			layers[i].style.zIndex = 8;
			var dataName = layers[i].getAttribute('data-name');
			if(name == dataName){
				layers[i].style.zIndex = this.options.zIndex;
			}
		};
	},
	small: function(index, small){
		var name = 'layer-box-n' + index;
			layer = this.idLayer.querySelector('.layer[data-name=' + name + ']');
		if(layer instanceof HTMLElement){
			if(small){
				layer.style.display = 'none';
			}else{
				layer.style.display = 'block';
			}
		}
	},
	size: function(obj, mouseDown){
		var w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth,
			h = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
		var width = obj.clientWidth,
			height = obj.clientHeight;
		if(width < w && height < h){
			obj.style.width = '100%';
			obj.style.height = (h - 35) + 'px';
			obj.style.top = 0;
			obj.style.left = 0;
			obj.querySelector('.size').setAttribute('class', 'iconfont icon-wind-zoom-out size');
		}else{
			obj.style.width = this.options.width + 'px';
			obj.style.height = this.options.height + 'px';
			obj.style.top = (h - this.options.height) / 2 + 'px';
			obj.style.left = (w - this.options.width) / 2 + 'px';
			obj.querySelector('.size').setAttribute('class', 'iconfont icon-chuangkou-fangda size');
		}
	},
	create: function(options){
		var that = this;
		// 创建移动块
		var popupLayerMove = document.createElement('div');
		popupLayerMove.setAttribute('class', 'popup-layer-move');
		// 标题
		var title = document.createElement('div');
		title.setAttribute('class', 'title');
		title.innerHTML = options.title;
		// 右上角按钮
		var buttons = document.createElement('div');
		buttons.setAttribute('class', 'buttons');
		// 最小化按钮
		var small = document.createElement('i');
		small.setAttribute('class', 'iconfont icon-zuixiaohua small');
		buttons.appendChild(small);
		// 最大化按钮
		var size = document.createElement('i');
		size.setAttribute('class', 'iconfont icon-chuangkou-fangda size');
		buttons.appendChild(size);
		// 关闭按钮
		var close = document.createElement('i');
		close.setAttribute('class', 'iconfont icon-guanbi close');
		buttons.appendChild(close);
		// 内容
		var main = document.createElement('div');
		main.setAttribute('class', 'main');
		main.style.height = options.height - 32 + 'px';
		// iframe
		var iframe = document.createElement('iframe');
		iframe.setAttribute('id', 'layerContent');
		iframe.src = options.href;
		iframe.setAttribute('allowtransparency', true);
		main.appendChild(iframe);
		// class layer
		var classLayer = document.createElement('div');
		classLayer.setAttribute('class', 'layer');
		classLayer.appendChild(popupLayerMove);
		classLayer.appendChild(title);
		classLayer.appendChild(buttons);
		classLayer.appendChild(main);
		classLayer.oncontextmenu = function(e){
			e.preventDefault();
		}
		classLayer.setAttribute('data-name', this.dataName);
		classLayer.setAttribute('data-index', options.index);
		classLayer.style.width = options.width + 'px';
		classLayer.style.height = options.height + 'px';
		classLayer.style.top = (this.height - options.height) / 2 + 'px';
		classLayer.style.left = (this.width - options.width) / 2 + 'px';
		var mouseDown = function(ev){
			ev.stopPropagation();
			var timer = null;
			clearTimeout(timer);
			var current = this;
			var layerMove = current.parentNode.querySelector('.popup-layer-move');
			layerMove.style.display = 'block';
			var parent = current.parentNode;
			var oevent = ev || event;
			var distanceX = oevent.clientX - parent.offsetLeft;
			var distanceY = oevent.clientY - parent.offsetTop;
			that.zIndex();
			that.options.level(parent.dataset.index);
			parent.style.zIndex = that.options.zIndex;
			document.onmousemove = function(ev){
				var oevent = ev || event;
				parent.style.left = oevent.clientX - distanceX + 'px';
				parent.style.top = oevent.clientY - distanceY + 'px'; 
			};
			var times = null;
			parent.onmouseup = function(){
				document.onmousemove = null;
				document.onmouseup = null;
				layerMove.style.display = 'none';
			};
		}
		classLayer.querySelector('.title').onmousedown = mouseDown;
		var timeer = null;
		classLayer.querySelector('.buttons .small').onclick = function(e){
			var index = this.parentNode.parentNode.dataset.index;
			that.small(index, true);
		}
		classLayer.querySelector('.buttons .size').onclick = function(e){
			that.size(this.parentNode.parentNode, mouseDown);
			var width = options.width,
				height = options.height;
			if(that.width > width && that.height > height){
				this.parentNode.parentNode.querySelector('.title').onmousedown = mouseDown;
			}else{
				this.parentNode.parentNode.querySelector('.title').onmousedown = null;
			}
		}
		classLayer.querySelector('.buttons .close').onclick = function(e){
			var index = this.parentNode.parentNode.dataset.index;
			that.close(index);
		}
		classLayer.ondblclick = function(ev){
			ev.stopPropagation();
			that.size(this, mouseDown);
			var width = this.clientWidth,
				height = this.clientHeight;
			if(that.width > width && that.height > height){
				this.querySelector('.title').onmousedown = mouseDown;
			}else{
				this.querySelector('.title').onmousedown = null;
			}
		};
				
		return classLayer;
	}
};

popup = new Popup();

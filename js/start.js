var start = {
	
	start: null,
	
	init: function(){
		this.start = document.querySelector('#start');
		var classStart = this.start.querySelector('.start');
		var that = this;
		classStart.onclick = function(){
			if(this.dataset.status == 'close'){
				that.open();
			}else{
				that.close();
			}
		};
		return this;
	},
	
	open: function(){
		this.start.querySelector('.start').dataset.status = 'open';
		this.start.querySelector('.start-menu').style.height = '400px';
	},
	
	close: function(){
		this.start.querySelector('.start').dataset.status = 'close';
		this.start.querySelector('.start-menu').style.height = 0;
	}
	
};
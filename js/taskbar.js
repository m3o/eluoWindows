function Taskbar(){
	this.width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
	this.height = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
	this.taskbar = document.querySelector('#taskbar');
	this.options = {
		title: null,
		index: null,
		close: function(){},
		level: function(){},
		small: function(){}
	};
	this.dataName = null;
};

Taskbar.prototype = {
	init: function(options){
		for(k in options){
			this.options[k] = options[k];
		};
		this.dataName = 'task-box-n' + this.options.index;
		var tasks = this.taskbar.querySelector('.mission .tasks');
		var isTask = this.taskbar.querySelector('.mission .tasks div[data-name=' + this.dataName + ']');
		if(!(isTask instanceof HTMLElement)){
			tasks.appendChild(this.create(this.options));
		}
		this.level(this.options.index);
	},
	remove: function(index){
		var name = 'task-box-n' + index;
		var task = this.taskbar.querySelector('.mission .tasks div[data-name=' + name + ']');
		if(task instanceof HTMLElement){
			task.parentNode.removeChild(task);
		}
	},
	level: function(index){
		var tasks = this.taskbar.querySelectorAll('.mission .tasks .task'),
			taskLen = tasks.length;
		for(var i=0; i<taskLen; i++){
			tasks[i].setAttribute('class', 'task');
		};
		var name = 'task-box-n' + index;
		var task = this.taskbar.querySelector('.mission .tasks div[data-name=' + name + ']');
		if(task instanceof HTMLElement){
			task.setAttribute('class', 'task current');
		};
	},
	menu: function(index, show){
		var that = this,
			offsetLeft = 0,
			name = 'task-box-n' + index,
			task = this.taskbar.querySelector('.mission .tasks div[data-name=' + name + ']');
		if(task instanceof HTMLElement){
			offsetLeft = task.offsetLeft;
		}
		var menu = this.taskbar.querySelector('.mission .task-right-menu'),
			close = menu.querySelector('.close');
		menu.style.left = offsetLeft + 'px';
		close.onclick = function(){
			that.remove(index);
			that.options.close(index);
			that.menu(index, false);
		};
		if(show == false){
			menu.style.display = 'none';
		}else{
			menu.style.display = 'block';
		}
	},
	create: function(options){
		var task = document.createElement('div'),
			that = this;
		task.setAttribute('class', 'task');
		task.setAttribute('data-name', this.dataName);
		task.setAttribute('data-index', options.index);
		task.innerText = options.title;
		task.oncontextmenu = function(){
			that.menu(this.dataset.index, true);
		};
		task.onclick = function(){
			var index = this.dataset.index;
			that.options.level(index);
			that.level(index);
			that.options.small(index);
		};
		return task;
	}
};

var taskbar = new Taskbar();
